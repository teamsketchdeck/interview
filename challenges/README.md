# Coding challenges

- Given array d , let delta(a,b) = Math.abs(a - b). Find me a number b such that _.all(d, (i) -> delta(i, b) == 5)
    - Test cases
    - [4, 14, 4]
    - [4, 14, 16]
    - [1, 3, 10, 16, 17]
    - [-15,-5,-5,20,30,30]
- Convert roman numerals into integers
- Transform the math library to take promised arguments and return promised answersWelcome to our repository of interview questions!

If you work on an exercise before the interview, let us know and make sure you have a very excellent answer :)