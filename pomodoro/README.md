
Howdy!

# v0 feature set

- List of people with their tomato
- Ability to add people
- Ability to toggle tomato

# v1 feature set

- Make it multiplayer so everyone shares the same state

# v2 feature set

- Allow for deleting people
- Make people appear/disappear in a nice way (slide in / grow / fade)

# v3 feature set

- Make the tomatoes run a 25 minute timer
 - Clicking on inactive starts the timer
 - Clicking on a running timer pauses it
 - Paused timer has a stop and play button, stop makes it inactive, play resumes the timer
- Display the timer either as a pie chart, or as a minute countdown e.g. 23min

# v4 feature set

- Include under each person's name their total tomato count for the last 7 days inclusive of today

# v5 feature set

- Host this online with a url /organisationName/ to get a board for any organisation that wants it
- Have a front page that gets new users to join an existing board or create one, and redirects existing users to their last opened board
- Have navigation on a board to get back to the homepage