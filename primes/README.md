
Howdy!

Let's create a prime number generator. The function should operate like this:

primes(3) = [1, 2, 5];

where its signature is:

Array[int] primes(howManyToGenerate: int);

# Definition

A handy reminder: the definition of a prime is that it is a number divisible only by 1 and itself. We're interested only in the set of positive integers.

# Exercise 1

Write the primes function and show it works

# Exercise 2

How efficient is your primes function? How could it be improved?

# Exercise 3

What is the most efficient primes function possible? Can you prove that efficiency? How long will it take to generate the first million prime numbers?